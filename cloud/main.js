require("cloud/app.js");
// Use AV.Cloud.define to define as many cloud functions as you want.
// For example:
AV.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});

AV.Cloud.beforeSave('CSULocation', function(request, response){
	var username = request.object.get("username");
	var current = new Date();
	var newDateObj = new Date(current.getTime() - 60000);
	var newDateString = newDateObj.toISOString();
	var cql = "select count(*) from CSULocation where username='"+username+"' and createdAt > date('"+newDateString+"')";
	console.log(cql);
	AV.Query.doCloudQuery(cql, {
	  success: function(result){
	    //results 是查询返回的结果，AV.Object 列表
	    var count = result.count;
	    if(count === 0 ){
	        request.object.set("timeout", 'true');
	    }
		response.success();
	  },
	  error: function(error){
	    //查询失败，查看 error
	    console.dir(error);
	  }
	});
});