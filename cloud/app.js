// 在 Cloud code 里初始化 Express 框架
var express = require('express');
var moment = require('moment');
var app = express();
var avosExpressCookieSession = require('avos-express-cookie-session');
var pageItemNum = 50;
var startDate;
var endDate;
var currentDateStr;
var Location = AV.Object.extend('CSULocation');

// App 全局配置
app.set('views','cloud/views');   // 设置模板目录
app.set('view engine', 'ejs');    // 设置 template 引擎
app.use(express.bodyParser());    // 读取请求 body 的中间件

// 启用 cookieParser
app.use(express.cookieParser('trackcsu'));
// 使用 avos-express-cookie-session 记录登录信息到 cookie
app.use(avosExpressCookieSession({ cookie: { maxAge: 3600000 },fetchUser:true}));

// 使用 Express 路由 API 服务 /hello 的 HTTP GET 请求
app.get('/', function(req, res) {
  res.redirect('/login');
});

app.get('/hello', function(req, res) {
  res.render('hello', { message: 'Congrats, you just set up your app!' });
});

app.get('/login', function(req, res) {
    // 渲染登录页面
    res.render('login');
});


app.get('/signup', function(req, res) {
    // 渲染注册页面
    res.render('signup');
});

// 点击注册页面的提交将出发下列函数
app.post('/signup', function(req, res) {
    var user = new AV.User();
    user.set("username", req.body.inputUsername);
    user.set("password", req.body.inputPassword);
    user.set("email", req.body.inputEmail);
    user.signUp(null, {
      success: function(user) {
        // Hooray! Let them use the app now.
        console.log('signup successfully!');
        res.redirect('/login');
      },
      error: function(user, error) {
        // Show the error message somewhere and let the user try again.
        console.log("Error: " + error.code + " " + error.message);
      }
    });
});

// 点击登录页面的提交将出发下列函数
app.post('/login', function(req, res) {
    AV.User.logIn(req.body.inputUsername, req.body.inputPassword).then(function() {
      //登录成功，avosExpressCookieSession会自动将登录用户信息存储到cookie
      //跳转到profile页面。
      console.log('signin successfully: %j', req.AV.user);
      res.redirect('/profile');
    },function(error) {
      //登录失败，跳转到登录页面
      res.redirect('/login');
  });
});
//查看用户profile信息
app.get('/profile', function(req, res) {
    // 判断用户是否已经登录 
  
    if (req.AV.user) { 
      var pageNum = req.query.p;
      var date = req.query.date;
      if (!pageNum) {
        pageNum = 1;
      }
      if (!date) { 
        date = new Date();
      }else{
        date = new Date(date);
      }
      currentDateStr = moment(date).zone("+08:00").format("YYYY-MM-DD HH:mm:ss");
      console.log(currentDateStr);
      currentDateStr = moment(date).zone("+08:00").format("YYYY-MM-DD");
      console.log(currentDateStr);
      startDate = new Date(date);
      startDate.setHours(0,0,0,0);
      endDate = new Date(date);
      endDate.setHours(23,59,59,999);
      console.log("startDate:"+startDate);
      console.log("endDate:"+endDate);
      var query = new AV.Query(Location);
      query.equalTo("timeout","true");
      query.greaterThanOrEqualTo("createdAt", startDate);
      query.lessThanOrEqualTo("createdAt", endDate);
      query.count({
        success: function(count) {
          // The count request succeeded. Show the count
          console.log("p:"+pageNum+",count: "+ count);
          renderProfile(res, req.AV.user,pageNum,count,currentDateStr);
        },
        error: function(error) {
          // The request failed
        }
      });
      // 如果已经登录，发送当前登录用户信息。
    } else {
      // 没有登录，跳转到登录页面。
      res.redirect('/login');
    }
});

//调用此url来登出帐号
app.get('/logout', function(req, res) {
  //avosExpressCookieSession将自动清除登录cookie信息
    AV.User.logOut();
    res.redirect('/profile');
});

function renderProfile(res, user,pageNum,count,date){
  var query = new AV.Query(Location);
  var skipNum = (pageNum-1)*pageItemNum;
  var totalPageNum = 0;
  if (count%pageItemNum == 0) {
    totalPageNum = count/pageItemNum;
  }else{
    totalPageNum = Math.floor(count/pageItemNum) + 1;
  }
  query.skip(skipNum);
  query.limit(pageItemNum);
  query.equalTo("timeout","true");
  query.greaterThanOrEqualTo("createdAt", startDate);
  query.lessThanOrEqualTo("createdAt", endDate);
  query.descending('createdAt');
  query.find({
    success: function(results){
      res.render('profile',{locations: results,user:user,totalPageNum:totalPageNum,pageNum:pageNum,date:date,moment:moment});
    },
    error: function(error){
      console.log(error);
    }
  });

}

// 最后，必须有这行代码来使 express 响应 HTTP 请求
app.listen();